from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path
from . import views

urlpatterns = [
    path('pest/<slug:slug>/', views.card, name='card_detail'),
    path('<slug:slug>/', views.page, name='page'),
    path('', views.index, name='index'),
    url(r'', include(('users.urls', 'users'), namespace='users')),
    path('api/request-editor/', views.editor_request, name='editor_request'),
    path('api/card/clarity-pos/', views.clarity_pos, name='clarity_pos'),
    path('api/card/clarity-neg/', views.clarity_neg, name='clarity_neg'),
]
