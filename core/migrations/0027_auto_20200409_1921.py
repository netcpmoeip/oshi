# Generated by Django 2.2.10 on 2020-04-09 19:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_auto_20200409_1917'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='indexnum',
            field=models.IntegerField(null=True, verbose_name='Индекс'),
        ),
    ]
