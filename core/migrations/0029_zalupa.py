# Generated by Django 2.2.10 on 2020-04-11 16:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_remove_card_index'),
    ]

    operations = [
        migrations.CreateModel(
            name='Zalupa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('zalupa', models.TextField(blank=True, verbose_name='Залупа')),
                ('card', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='zalupas', to='core.Card')),
            ],
            options={
                'verbose_name': 'Залупа',
                'verbose_name_plural': 'Залупы',
            },
        ),
    ]
