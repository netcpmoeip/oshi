# Generated by Django 2.2.10 on 2020-02-17 20:26

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0016_auto_20200217_1038'),
    ]

    operations = [
        migrations.AlterField(
            model_name='card',
            name='description',
            field=ckeditor.fields.RichTextField(verbose_name='Описание'),
        ),
        migrations.AlterField(
            model_name='card',
            name='example',
            field=ckeditor.fields.RichTextField(blank=True, verbose_name='Дополнительные примеры (потренируйтесь на них)'),
        ),
        migrations.AlterField(
            model_name='card',
            name='fix',
            field=ckeditor.fields.RichTextField(verbose_name='Как побороть ошибку?'),
        ),
        migrations.AlterField(
            model_name='card',
            name='info',
            field=ckeditor.fields.RichTextField(blank=True, verbose_name='Что еще прочесть?'),
        ),
        migrations.AlterField(
            model_name='page',
            name='content',
            field=ckeditor.fields.RichTextField(verbose_name='Контент'),
        ),
    ]
